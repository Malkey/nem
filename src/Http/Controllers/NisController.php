<?php

namespace gladwelln\nem\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;
use NemAPI;

class NisController extends Controller
{
    public function __construct() { }

    public function check_nis_heartbeat()
    {
        $response = NemAPI::getJson('/heartbeat');
        if(!$response['status'])
        {
            return Response::json($response);
        }

        $html = view('nem::elements.nis-response', compact('response'))->render();
        
        return Response::json(['status' => true, 'content' => $html]);
    }

    public function check_nis_status()
    {
        $response = NemAPI::getJson('/status');
        if(!$response['status'])
        {
            return Response::json($response);
        }

        $html = view('nem::elements.nis-response', compact('response'))->render();
        
        return Response::json(['status' => true, 'content' => $html]);
    }

    public function get_block_chain_info($action)
    {
        $response = NemAPI::getJson("/chain/$action");
        if(!$response['status'])
        {
            return Response::json($response);
        }
        
        $html = view('nem::elements.nis-response', compact('response'))->render();
        
        return Response::json(['status' => true, 'content' => $html]);
    }

    public function get_block_chain_height_form($block)
    {
        $html = view('nem::elements.nis.height-form', compact('block'))->render();
        
        return Response::json(['status' => true, 'content' => $html]);
    }

    public function get_block_chain_height_data(Request $request)
    {
        $block = $request->get('block');
        $height = $request->get('height');

        $url = ($block == 'full') ? '/block/at/public' : '/local/chain/blocks-after';

        $response = NemAPI::postJson($url, [], ['height' => $height]);
        if(!$response['status'])
        {
            return Response::json($response);
        }
        
        $html = view('nem::elements.nis-response', compact('response'))->render();
        
        return Response::json(['status' => true, 'content' => $html]);
    }

    public function get_node_info($action)
    {
        $response = NemAPI::getJson("/node/$action");
        if(!$response['status'])
        {
            return Response::json($response);
        }
        
        $html = view('nem::elements.nis-response', compact('response'))->render();
        
        return Response::json(['status' => true, 'content' => $html]);
    }

    public function get_peer_nodes($which_nodes)
    {
        $response = NemAPI::getJson("/node/peer-list/$which_nodes");
        if(!$response['status'])
        {
            return Response::json($response);
        }
        
        $html = view('nem::elements.nis-response', compact('response'))->render();
        
        return Response::json(['status' => true, 'content' => $html]);
    }

    public function get_active_peer_nodes_height()
    {
        $response = NemAPI::getJson("/node/active-peers/max-chain-height");
        if(!$response['status'])
        {
            return Response::json($response);
        }
        
        $html = view('nem::elements.nis-response', compact('response'))->render();
        
        return Response::json(['status' => true, 'content' => $html]);
    }

    public function get_node_experiences()
    {
        $response = NemAPI::getJson("/node/experiences");
        if(!$response['status'])
        {
            return Response::json($response);
        }
        
        $html = view('nem::elements.nis-response', compact('response'))->render();
        
        return Response::json(['status' => true, 'content' => $html]);
    }

    public function boot_local_node()
    {
        $data = [
            'metaData' => [
                'application' => 'NIS'
            ],
            'endpoint' => [
                'protocol' => 'http',
                'port' => 7890,
                'host' => 'localhost'
            ],
            'identity' => [
                'private-key' => env('TEST_PRVT_KEY', '********'),
                'name' => 'G-Dawg'
            ]
        ];

        $response = NemAPI::postJson("/node/boot", [], $data);
        if(!$response['status'])
        {
            return Response::json($response);
        }
        
        $html = view('nem::elements.nis-response', compact('response'))->render();
        
        return Response::json(['status' => true, 'content' => $html]);
    }

    public function get_time_synchronization()
    {
        $response = NemAPI::getJson("/debug/time-synchronization");
        if(!$response['status'])
        {
            return Response::json($response);
        }
        
        $html = view('nem::elements.nis-response', compact('response'))->render();
        
        return Response::json(['status' => true, 'content' => $html]);
    }

    public function get_calls($calls)
    {
        $response = NemAPI::getJson("/debug/connections/$calls");
        if(!$response['status'])
        {
            return Response::json($response);
        }
        
        $html = view('nem::elements.nis-response', compact('response'))->render();
        
        return Response::json(['status' => true, 'content' => $html]);
    }

    public function get_timers()
    {
        $response = NemAPI::getJson("/debug/timers");
        if(!$response['status'])
        {
            return Response::json($response);
        }
        
        $html = view('nem::elements.nis-response', compact('response'))->render();
        
        return Response::json(['status' => true, 'content' => $html]);
    }
}