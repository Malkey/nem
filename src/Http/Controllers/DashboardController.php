<?php

namespace gladwelln\nem\Http\Controllers;

use App\Http\Controllers\Controller;
use Session;
use NemAPI;

class DashboardController extends Controller
{
    public function __construct() { }

    public function index()
    {
        //Session::flash('flash_success', 'Hello and welcome to my nem development quick start.');
        //$response = NemAPI::getJson('/account/transfers/incoming', ['address' => 'TAVD6PIKUB3YXQMM6NAU3UGU63E7ZUYKK6B6ULJV']);
        //print_r($response);
        return view('nem::dashboard');
    }
}