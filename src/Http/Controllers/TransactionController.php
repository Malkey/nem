<?php

namespace gladwelln\nem\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;
use NemAPI;

class TransactionController extends Controller
{
    public function __construct() { }

    public function get_transaction_form($using)
    {
        $html = view('nem::elements.transaction.' . $using . '_form', compact('using'))->render();
        
        return Response::json(['status' => true, 'content' => $html]);
    }

    public function submit_transaction(Request $request)
    {
        $data = $request->get('data');
        $using = $data['using'];
        $recipient = $data['recipient'];
        $amount = isset($data['amount']) ? $data['amount'] : 1;
        $quantity = isset($data['quantity']) ? $data['quantity'] : 0;
        $mosaic = isset($data['mosaic']) ? explode(':', $data['mosaic'], 2) : [];
        $message = $data['message'];
        $fee = $data['fee'];
    
        $private_key = env('TEST_PRVT_KEY', '*******');
        $public_key = env('TEST_PBLC_KEY', '*******');

        $transaction = [
            'transaction' => [
                'timeStamp'    => (time() - 1427587585),
                'amount'       => ($amount * 1000000),
                'fee'          => ($fee * 1000000),
                'recipient'    => str_replace('-', '', $recipient),
                'type'         => 257,
                'deadline'     => (time() - 1427587585 + 43200),
                'message'      => !strlen($message) ? null : ['payload' => bin2hex($message), 'type' => 1],
                'version'      => ($using == 'version1') ? -1744830463 : -1744830462,
                'signer'       => $public_key
            ],
            'privateKey' => $private_key
        ];
        
        if($using === "version2")
        {
            $response = NemAPI::getJson("/namespace/mosaic/definition/page", ['namespace' => $mosaic[0]]);
            if($response['status'] == false)
            {
                return Response::json($response);
            }

            $data = json_decode($response['payload'], true)['data'];
            
            //Validate namespace
            if(empty($data))
            {
                return Response::json(['status' => false, 'error_description' => 'Could not find mosaic definition for namespace: ' . $mosaic[0]]);
            }

            //Validate mosaic
            $found = 0;
            foreach($data as $index)
            {
                if($mosaic[0] == $index['mosaic']['id']['namespaceId'])
                {
                    if(isset($mosaic[1]) && ($mosaic[1] == $index['mosaic']['id']['name']))
                    {
                        $found++;
                    }
                }
            }
            
            if(!$found)
            {
                return Response::json(['status' => false, 'error_description' => 'Could not find mosaic under namespace: ' . $mosaic[0]]);
            }
            
            $transaction['transaction']['mosaics'] = [
                [
                    'mosaicId' => [
                        'namespaceId' => $mosaic[0],
                        'name' => $mosaic[1]
                    ],
                    'quantity' => ($quantity * 1000000)
                ]
            ];
        }
        
        $response = NemAPI::postJson("/transaction/prepare-announce", [], $transaction);
        if(!$response['status'])
        {
            return Response::json($response);
        }

        $html = view('nem::elements.nis-response', compact('response'))->render();
        
        return Response::json(['status' => true, 'content' => $html]);
    }

    public function get_account_historical_data_form()
    {
        $html = view('nem::elements.account.historical-data-form')->render();
        
        return Response::json(['status' => true, 'content' => $html]);
    }

    public function get_account_historical_data(Request $request)
    {
        $data = $request->get('data');
        $address = str_replace('-', '', $data['address']);
        $start_height = $data['start-height'];
        $end_height = $data['end-height'];
        $increment = ($data['increment']) ? $data['increment'] : 1;
        
        $params['address'] = $address;
        if($start_height)
        {
            $params['startHeight'] = $start_height;
        }
        
        if($end_height)
        {
            $params['endHeight'] = $end_height;
        }

        $response = NemAPI::getJson("/account/historical/get", $params);
        if(!$response['status'])
        {
            return Response::json($response);
        }

        $html = view('nem::elements.nis-response', compact('response'))->render();
        
        return Response::json(['status' => true, 'content' => $html]);
    }

    public function submit_multisig_transaction(Request $request)
    {
        $data = $request->get('data');
        $mosaic_transfer = $data['mosaic_transfer'];
        $recipient = $data['recipient'];
        $amount = isset($data['amount']) ? $data['amount'] : 1;
        $quantity = isset($data['quantity']) ? $data['quantity'] : 0;
        $mosaic = isset($data['mosaic']) ? explode(':', $data['mosaic'], 2) : [];
        $message = $data['message'];
        $fee = $data['fee'];
        $multisig_fee = 0.15;

        $cosig_private_key = env('TEST_PRVT_KEY', '*******');
        $cosig_public_key = env('TEST_PBLC_KEY', '*******');
        $multisig_public_key = env('MULTISIG_PBLC_KEY', '*******');

        $transaction = [
            'transaction' => [
                'timeStamp'    => (time() - 1427587585),
                'fee'          => ($multisig_fee * 1000000),
                'type'         => 4100,
                'deadline'     => (time() - 1427587585 + 43200),
                'version'      => -1744830463,
                'signer'       => $cosig_public_key,
                'otherTrans'   => [
                    'timeStamp'    => (time() - 1427587585),
                    'amount'       => ($amount * 1000000),
                    'fee'          => ($fee * 1000000),
                    'recipient'    => str_replace('-', '', $recipient),
                    'type'         => 257,
                    'deadline'     => (time() - 1427587585 + 43200),
                    'message'      => !strlen($message) ? null : ['payload' => bin2hex($message), 'type' => 1],
                    'version'      => -1744830463,
                    'signer'       => $multisig_public_key,
                ],
                'signatures' => []
            ],
            'privateKey' => $cosig_private_key
        ];
        
        if($mosaic_transfer)
        {
            $response = NemAPI::getJson("/namespace/mosaic/definition/page", ['namespace' => $mosaic[0]]);
            if($response['status'] == false)
            {
                return Response::json($response);
            }

            $data = json_decode($response['payload'], true)['data'];
            
            //Validate namespace
            if(empty($data))
            {
                return Response::json(['status' => false, 'error_description' => 'Could not find mosaic definition for namespace: ' . $mosaic[0]]);
            }

            //Validate mosaic
            $found = 0;
            foreach($data as $index)
            {
                if($mosaic[0] == $index['mosaic']['id']['namespaceId'])
                {
                    if(isset($mosaic[1]) && ($mosaic[1] == $index['mosaic']['id']['name']))
                    {
                        $found++;
                    }
                }
            }
            
            if(!$found)
            {
                return Response::json(['status' => false, 'error_description' => 'Could not find mosaic under namespace: ' . $mosaic[0]]);
            }
            
            $transaction['transaction']['otherTrans']['mosaics'] = [
                [
                    'mosaicId' => [
                        'namespaceId' => $mosaic[0],
                        'name' => $mosaic[1]
                    ],
                    'quantity' => ($quantity * 1000000)
                ]
            ];
        }
        
        $response = NemAPI::postJson("/transaction/prepare-announce", [], $transaction);
        if(!$response['status'])
        {
            return Response::json($response);
        }

        $html = view('nem::elements.nis-response', compact('response'))->render();
        
        return Response::json(['status' => true, 'content' => $html]);
    }
}