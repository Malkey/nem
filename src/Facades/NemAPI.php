<?php

namespace gladwelln\nem\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @see \gladwelln\nem\NemAPIManager
 */
class NemAPI extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'NemAPI';
    }
}
