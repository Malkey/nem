jQuery.extend({
    doAJAX: function (url, data, type, callback)
    {
        if (type.toLowerCase() != "get")
        {
            data["_token"] = _token;
        }

        type = !(type) ? "GET" : type;

        return $.ajax({
            type: type,
            url: url,
            data: data,
            dataType: "json",
            error: function (XMLHttpRequest, textStatus, errorThrown)
            {
                toastr.clear();
                notify('error',"Error Code: " + XMLHttpRequest.status + " : " + XMLHttpRequest.statusText);
            },
            success: function (data)
            {
                update_layout();
                
                callback(data);
            }
        });
    }
});

$.fn.extend({
    treed: function (o) {
    
        var openedClass = 'glyphicon-minus-sign';
        var closedClass = 'glyphicon-plus-sign';
        
        if (typeof o != 'undefined'){
            if (typeof o.openedClass != 'undefined'){
            openedClass = o.openedClass;
            }
            if (typeof o.closedClass != 'undefined'){
            closedClass = o.closedClass;
            }
        };
    
        //initialize each of the top levels
        var tree = $(this);
        tree.addClass("tree");
        tree.find('li').has("ul").each(function () {
            var branch = $(this); //li with children ul
            branch.prepend("<i class='indicator glyphicon " + closedClass + "'></i>");
            branch.addClass('branch');
            branch.on('click', function (e) {
                if (this == e.target) {
                    var icon = $(this).children('i:first');
                    icon.toggleClass(openedClass + " " + closedClass);
                    $(this).children().children().toggle();
                }
            })
            branch.children().children().toggle();
        });
        //fire event from the dynamically added icon
        tree.find('.branch .indicator').each(function(){
            $(this).on('click', function () {
                $(this).closest('li').click();
            });
        });
        //fire event to open branch if the li contains an anchor instead of text
        tree.find('.branch>a').each(function () {
            $(this).on('click', function (e) {
                $(this).closest('li').click();
                e.preventDefault();
            });
        });
        //fire event to open branch if the li contains a button instead of text
        tree.find('.branch>button').each(function () {
            $(this).on('click', function (e) {
                $(this).closest('li').click();
                e.preventDefault();
            });
    
        });
    }
});

function notify(type, msg, title, time_out)
{
    time_out = (time_out == undefined) ? 0 : time_out;

    toastr.options = {
        "closeButton": true,
        "closeHtml": "<i class='glyphicon glyphicon-remove-sign'></i>",
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": 300,
        "hideDuration": 300,
        "timeOut": time_out,
        "extendedTimeOut": 0,
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    toastr[type](msg, title);
}

$(document).ready(function()
{
    update_layout();
});

function update_layout()
{
    $('#content-holder').css('height', $(window).height() - 40);
    $('#content-holder').css('overflow', 'scroll');
}