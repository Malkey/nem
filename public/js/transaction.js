function getTransactionForm(using)
{
    $.doAJAX(base_url + '/ajax_get_transaction_form/' + using, {}, 'GET', function (response)
    {
        if (response.status == true)
        {
            $('#content-holder').fadeOut('slow').empty().append(response.content).fadeIn('slow');
        }
        else
        {
            notify("error", response.error_description);
        }
    });

    return false;
}

function calculateFee(isMultisig)
{
    var amount = ($('#amount').length) ? $('#amount').val() : $('#quantity').val();
    var message = $('#message').val();
    var fee = 1.25;
    
    /*Fees for transferring XEM to another account:
    0.05 XEM per 10,000 XEM transferred, capped at 1.25 XEM
    Example:
    0.20 XEM fee for a 45,000 XEM transfer, 1.25 XEM fee for a 500,000 XEM transfer.*/
    if(amount < 500000)
    {
        fee = Math.floor(amount / 10000);
        fee = (fee) ? (0.05 * fee) : 0.05;
    }

    /*Fees for appending a message to a transaction:
    0.05 XEM per commenced 32 message bytes (messageLength / 32 + 1).*/
    if(message.length)
    {
        var num = Math.floor(message.length / 32) + 1;
        fee += (0.05 * num);
    }

    var extra = (isMultisig) ? '0.150000 + ' : '';

    $('#fee').val(fee.toFixed(2));
    $('#fee_read_only').val(extra + fee.toFixed(6));
}

function submitTransactionForm(using)
{
    var data = {using: using};
    var recipient = $('#recipient').val();
    var mosaic = $('#mosaic').val();

    if(!recipient)
    {
        notify("warning", 'Please provide a recipient to continue!');
        return false;
    }
    if($('#mosaic').length && !mosaic)
    {
        notify("warning", 'Please provide a mosaic to continue!');
        return false;
    }

    $('form.form-horizontal .form-control').each(function()
    {
        data[$(this).attr('id')] = $(this).val();
    });

    $.doAJAX(base_url + '/ajax_submit_transaction', {data: data}, 'POST', function (response)
    {
        if (response.status == true)
        {
            $('#transaction-content-holder').fadeOut('slow').empty().append(response.content).fadeIn('slow');
        }
        else
        {
            notify("error", response.error_description);
        }
    });

    return false;
}

function sendMultisigTransaction()
{
    var mosaic_transfer = ($(".mosaic-checkbox").is(':checked')) ? 1 : 0;
    var data = {'mosaic_transfer': mosaic_transfer};

    if(!$('#recipient').val())
    {
        notify("warning", 'Please provide a recipient to continue!');
        return false;
    }

    if(mosaic_transfer)
    {
        if(!$('#mosaic').val())
        {
            notify("warning", 'Please provide a mosaic to continue!');
            return false;
        }
    }

    $('form.form-horizontal .form-control').each(function()
    {
        data[$(this).attr('id')] = $(this).val();
    });
    
    $.doAJAX(base_url + '/ajax_submit_multisig_transaction', {data: data}, 'POST', function (response)
    {
        if (response.status == true)
        {
            $('#transaction-content-holder').fadeOut('slow').empty().append(response.content).fadeIn('slow');
        }
        else
        {
            notify("error", response.error_description);
        }
    });

    return false;
}