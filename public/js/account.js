function generateNewAccount()
{
    $.doAJAX(base_url + '/ajax_generate_new_account', {}, 'GET', function (response)
    {
        if (response.status == true)
        {
            $('#content-holder').fadeOut('slow').empty().append(response.content).fadeIn('slow');
        }
        else
        {
            notify("error", response.error_description);
        }
    });

    return false;
}

function getAccountForm(using)
{
    $.doAJAX(base_url + '/ajax_get_account_form/' + using, {}, 'GET', function (response)
    {
        if (response.status == true)
        {
            $('#content-holder').fadeOut('slow').empty().append(response.content).fadeIn('slow');
        }
        else
        {
            notify("error", response.error_description);
        }
    });

    return false;
}

function getAccountData(using)
{
    var value = $('#' + using).val();

    if(!value)
    {
        notify("warning", 'Please provide a value to continue!');
        return false;
    }

    $.doAJAX(base_url + '/ajax_get_account_data', {'using': using, 'value': value}, 'POST', function (response)
    {
        if (response.status == true)
        {
            $('#account-content-holder').fadeOut('slow').empty().append(response.content).fadeIn('slow');
        }
        else
        {
            notify("error", response.error_description);
        }
    });

    return false;
}

function getAccountTransactionForm(using)
{
    $.doAJAX(base_url + '/ajax_get_account_transaction_form/' + using, {}, 'GET', function (response)
    {
        if (response.status == true)
        {
            $('#content-holder').fadeOut('slow').empty().append(response.content).fadeIn('slow');
        }
        else
        {
            notify("error", response.error_description);
        }
    });

    return false;
}

function getAccountTransactionData(using)
{
    var address = $('#address').val();
    var hash = $('#hash').val();

    if(!address)
    {
        notify("warning", 'Please provide a address to continue!');
        return false;
    }

    $.doAJAX(base_url + '/ajax_get_account_transaction_data', {'using': using, 'address': address, 'hash': hash}, 'POST', function (response)
    {
        if (response.status == true)
        {
            $('#account-content-holder').fadeOut('slow').empty().append(response.content).fadeIn('slow');
        }
        else
        {
            notify("error", response.error_description);
        }
    });

    return false;
}

function getAccountImportances()
{
    $.doAJAX(base_url + '/ajax_get_account_importances', {}, 'GET', function (response)
    {
        if (response.status == true)
        {
            $('#content-holder').fadeOut('slow').empty().append(response.content).fadeIn('slow');
        }
        else
        {
            notify("error", response.error_description);
        }
    });

    return false; 
}

function lockUnlockAccount(action)
{
    $.doAJAX(base_url + '/ajax_lock_unclock_account/' + action, {}, 'GET', function (response)
    {
        if (response.status == true)
        {
            $('#content-holder').fadeOut('slow').empty().append(response.content).fadeIn('slow');
        }
        else
        {
            notify("error", response.error_description);
        }
    });

    return false; 
}

function getAccountUnlockInfo()
{
    $.doAJAX(base_url + '/ajax_get_account_unclock_info', {}, 'GET', function (response)
    {
        if (response.status == true)
        {
            $('#content-holder').fadeOut('slow').empty().append(response.content).fadeIn('slow');
        }
        else
        {
            notify("error", response.error_description);
        }
    });

    return false; 
}

function getAccountNamespaceForm()
{
    $.doAJAX(base_url + '/ajax_get_account_namespace_form/' + using, {}, 'GET', function (response)
    {
        if (response.status == true)
        {
            $('#content-holder').fadeOut('slow').empty().append(response.content).fadeIn('slow');
        }
        else
        {
            notify("error", response.error_description);
        }
    });

    return false;
}

function getAccountNamespaceForm()
{
    $.doAJAX(base_url + '/ajax_get_account_namespace_form', {}, 'GET', function (response)
    {
        if (response.status == true)
        {
            $('#content-holder').fadeOut('slow').empty().append(response.content).fadeIn('slow');
        }
        else
        {
            notify("error", response.error_description);
        }
    });

    return false;
}

function getAccountNamespaceData()
{
    var address = $('#address').val();
    var parent = $('#paregetAccountHistoricalDataFormnt').val();

    if(!address)
    {
        notify("warning", 'Please provide a address to continue!');
        return false;
    }

    $.doAJAX(base_url + '/ajax_get_account_namespace_data', {'parent': parent, 'address': address}, 'POST', function (response)
    {
        if (response.status == true)
        {
            $('#account-content-holder').fadeOut('slow').empty().append(response.content).fadeIn('slow');
        }
        else
        {
            notify("error", response.error_description);
        }
    });

    return false;
}

function getAccountMosaicDefinitionsForm()
{
    $.doAJAX(base_url + '/ajax_get_account_mosaic_definitions_form', {}, 'GET', function (response)
    {
        if (response.status == true)
        {
            $('#content-holder').fadeOut('slow').empty().append(response.content).fadeIn('slow');
        }
        else
        {
            notify("error", response.error_description);
        }
    });

    return false;
}

function getAccountMosaicDefinitionsData()
{
    var address = $('#address').val();
    var parent = $('#parent').val();

    if(!address)
    {
        notify("warning", 'Please provide a address to continue!');
        return false;
    }

    $.doAJAX(base_url + '/ajax_get_account_mosaic_definitions_data', {'parent': parent, 'address': address}, 'POST', function (response)
    {
        if (response.status == true)
        {
            $('#account-content-holder').fadeOut('slow').empty().append(response.content).fadeIn('slow');
        }
        else
        {
            notify("error", response.error_description);
        }
    });

    return false;
}

function getAccountMosaicsForm()
{
    $.doAJAX(base_url + '/ajax_get_account_mosaics_form', {}, 'GET', function (response)
    {
        if (response.status == true)
        {
            $('#content-holder').fadeOut('slow').empty().append(response.content).fadeIn('slow');
        }
        else
        {
            notify("error", response.error_description);
        }
    });

    return false;
}

function getAccountMosaicsData()
{
    var address = $('#address').val();

    if(!address)
    {
        notify("warning", 'Please provide a address to continue!');
        return false;
    }

    $.doAJAX(base_url + '/ajax_get_account_mosaics_data', {'address': address}, 'POST', function (response)
    {
        if (response.status == true)
        {
            $('#account-content-holder').fadeOut('slow').empty().append(response.content).fadeIn('slow');
        }
        else
        {
            notify("error", response.error_description);
        }
    });

    return false;
}

function getAccountHistoricalDataForm()
{
    $.doAJAX(base_url + '/ajax_get_account_historical_data_form', {}, 'GET', function (response)
    {
        if (response.status == true)
        {
            $('#content-holder').fadeOut('slow').empty().append(response.content).fadeIn('slow');
        }
        else
        {
            notify("error", response.error_description);
        }
    });

    return false;
}

function getAccountHistoricalData()
{
    var data = {};
    var address = $('#address').val();
    
    if(!address)
    {
        notify("warning", 'Please provide a address to continue!');
        return false;
    }

    $('form.form-horizontal .form-control').each(function()
    {
        data[$(this).attr('id')] = $(this).val();
    });

    $.doAJAX(base_url + '/ajax_get_account_historical_data', {data: data}, 'POST', function (response)
    {
        if (response.status == true)
        {
            $('#historical-content-holder').fadeOut('slow').empty().append(response.content).fadeIn('slow');
        }
        else
        {
            notify("error", response.error_description);
        }
    });

    return false;
}