function getRootNamespaceForm()
{
    $.doAJAX(base_url + '/ajax_get_root_namespace_form', {}, 'GET', function (response)
    {
        if (response.status == true)
        {
            $('#content-holder').fadeOut('slow').empty().append(response.content).fadeIn('slow');
        }
        else
        {
            notify("error", response.error_description);
        }
    });

    return false;
}

function getRootNamespaceData()
{
    var id = $('#id').val();
    var pagesize = $('#pagesize').val();
    
    $.doAJAX(base_url + '/ajax_get_root_namespace_data', {'pagesize': pagesize, 'id': id}, 'POST', function (response)
    {
        if (response.status == true)
        {
            $('#namespace-content-holder').fadeOut('slow').empty().append(response.content).fadeIn('slow');
        }
        else
        {
            notify("error", response.error_description);
        }
    });

    return false;
}

function getNamespaceForm()
{
    $.doAJAX(base_url + '/ajax_get_namespace_form', {}, 'GET', function (response)
    {
        if (response.status == true)
        {
            $('#content-holder').fadeOut('slow').empty().append(response.content).fadeIn('slow');
        }
        else
        {
            notify("error", response.error_description);
        }
    });

    return false;
}

function getNamespaceData()
{
    var namespace = $('#namespace').val();
  
    $.doAJAX(base_url + '/ajax_get_namespace_data', {'namespace': namespace}, 'POST', function (response)
    {
        if (response.status == true)
        {
            $('#namespace-content-holder').fadeOut('slow').empty().append(response.content).fadeIn('slow');
        }
        else
        {
            notify("error", response.error_description);
        }
    });

    return false;
}

function getMosaicDefinitonsForm()
{
    $.doAJAX(base_url + '/ajax_get_mosaic_definitions_form', {}, 'GET', function (response)
    {
        if (response.status == true)
        {
            $('#content-holder').fadeOut('slow').empty().append(response.content).fadeIn('slow');
        }
        else
        {
            notify("error", response.error_description);
        }
    });

    return false;
}

function getMosaicDefinitonsData()
{
    var namespace = $('#namespace').val();
    var id = $('#id').val();
    var pagesize = $('#pagesize').val();
    
    if(!namespace)
    {
        notify("warning", 'Please provide a namespace to continue!');
        return false;
    }

    $.doAJAX(base_url + '/ajax_get_mosaic_definitions_data', {'namespace': namespace, 'id': id, 'pagesize': pagesize}, 'POST', function (response)
    {
        if (response.status == true)
        {
            $('#namespace-content-holder').fadeOut('slow').empty().append(response.content).fadeIn('slow');
        }
        else
        {
            notify("error", response.error_description);
        }
    });

    return false;
}