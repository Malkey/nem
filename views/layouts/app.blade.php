<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{!! $title or config('app.name', 'Package Development Quick Start') !!}</title>

    <!-- Styles -->
    {!! Html::style(url('vendor/nem/css/bootstrap.min.css')) !!}
    {!! Html::style(url('vendor/nem/css/toastr.css')) !!}
    {!! Html::style(url('vendor/nem/css/sweetalert.css')) !!}
    {!! Html::style(url('vendor/nem/css/common.css')) !!}

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    @stack('style')

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <!-- Scripts -->
    {!! Html::script(url('vendor/nem/js/jquery-3.1.0.js')) !!}
    {!! Html::script(url('vendor/nem/js/jquery-blockui.js')) !!}
    {!! Html::script(url('vendor/nem/js/bootstrap.min.js')) !!}
    {!! Html::script(url('vendor/nem/js/jquery-ui.js')) !!}
    {!! Html::script(url('vendor/nem/js/toastr.min.js')) !!}
    {!! Html::script(url('vendor/nem/js/core.js')) !!}

    <script type="text/javascript">
        window.onbeforeunload = function (e)
        {
            $.blockUI({message: '<img src="{!! asset('vendor/nem/img/loader.gif') !!}">', css: { backgroundColor: 'transparent', border: 'none', cursor: 'wait' }, baseZ: 999999999});
        };

        $(document).ajaxStart(function ()
        {
            $.blockUI({message: '<img src="{!! asset('vendor/nem/img/loader.gif') !!}">', css: { backgroundColor: 'transparent', border: 'none', cursor: 'wait' }, baseZ: 999999999});
        }).ajaxStop(function ()
        {
            $.unblockUI();
        });

        //Global viriables
        var base_url = "{!! url('') !!}";
        var _token = "{{ csrf_token() }}";
    </script>
</head>
<body>
    
    @yield('content')

    <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="nem-global-mdl">
        {{--global modal--}}
    </div>

    @stack('scripts')

    @if(Session::has('flash_error'))

        <script type="text/javascript">
            notify('error', '{!! Session::get('flash_error') !!}', '', 0);
        </script>

    @endif

    @if(Session::has('flash_warning'))

        <script type="text/javascript">
            notify('warning', '{!! Session::get('flash_warning') !!}', '', 0);
        </script>

    @endif

    @if(Session::has('flash_success'))

        <script type="text/javascript">
            notify('success', '{!! Session::get('flash_success') !!}');
        </script>

    @endif

    @if(Session::has('flash_info'))

        <script type="text/javascript">
            notify('info', '{!! Session::get('flash_info') !!}', '', 0);
        </script>

    @endif

</body>
</html>