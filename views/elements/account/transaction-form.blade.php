<div class="panel panel-default">
    <div class="panel-heading">
        <div class="input-group">
            <div class="row">
                <div class="col-md-6">
                    <input type="text" class="form-control input-lg" id="address" placeholder="Enter your address here...">
                </div>
                <div class="col-md-6">
                    <input type="text" class="form-control input-lg" id="hash" placeholder="Enter hash here...">
                </div>
            </div>
            <span class="input-group-btn">
                <button class="btn btn-success btn-lg" type="button" onclick="getAccountTransactionData('{!! $using !!}')">Go!</button>
            </span>
        </div>
    </div>
    <div class="panel-body" id="account-content-holder">
        <h3 class="text-info text-center">Enter your {!! ($using == 'public_key') ? 'public key' : 'address' !!} and or hash above to continue.</h3>
    </div>
</div>