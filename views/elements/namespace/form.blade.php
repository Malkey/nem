<div class="panel panel-default">
    <div class="panel-heading">
        <div class="input-group">
            <input type="text" class="form-control input-lg" id="namespace" placeholder="Enter your namespace here...">
            <span class="input-group-btn">
                <button class="btn btn-success btn-lg" type="button" onclick="getNamespaceData()">Go!</button>
            </span>
        </div>
    </div>
    <div class="panel-body" id="namespace-content-holder">
        <h3 class="text-info text-center">Enter your namespace above to continue.</h3>
    </div>
</div>